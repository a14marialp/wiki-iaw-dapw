﻿


> Written with [StackEdit](https://stackedit.io/).
> 

 1. En Linux descargaremos las herramientas desde la linea de comandos con apt install php

    *sudo apt install apache2 php7.2 php7.2-cli php7.2-common php7.2-mysql libapache2-mod-php7.2*
    

 2. Dónde tenemos los ficheros de configuración de apache?  
 3. Dónde se almacena por defecto los recursos web en apache?

 4. Cómo compruebo que el servidor web está funcionando?
 
 5. Dónde tenemos los ficheros de configuración de mysql?   
 6.  Dónde tenemos los ficheros de datos de mysql?    
 7. Accede a mysql y crea usuario con tu nombre
 8. Descargar y descomprimir phpmyadmin en un directorio. Como root copiar el directorio resultante en /var/www/html
 9. Ir a la url localhost/phpmyadmin para abrir el administrador php de la base de datos mysql
 10.Crea una nueva base de datos en mysql con phpmyadmin. Comprueba desde terminal que se ha creado correctamente.

