﻿


> Written with [StackEdit](https://stackedit.io/).
>  # Errores típicos.

 - Tu cliente  FTP  muestra el error  **access denied**, o similar, cuando subes o borras ficheros y carpetas: Comprueba que tu usuario  FTP  tenga permisos suficientes sobre la carpeta o fichero en la que deseas subir o que deseas borrar.
 - Tus páginas no son reconocidas de forma automática al acceder a tu dominio: Los servidores  GNU/Linux son sensibles a mayúsculas y minúsculas por lo que verifica el nombre de tus archivos.
 - El cliente de  FTP  te muestra el mensaje  **too many connections from your IP address**: Esto quiere decir que existen más conexiones abiertas con el servidor  FTP  desde la misma dirección  IP  de las permitidas. En ese caso, asegúrate que no exista ninguna aplicación, como un cortafuegos, que pueda estar bloqueando las conexiones abiertas, y provocando, de esta forma, que se establezcan más intentos de conexión de los necesarios.
