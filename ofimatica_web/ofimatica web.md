﻿
# Introdución ás aplicacións ofimáticas web

As aplicacións ofimáticas web (ou aplicacións na_nube_) son aquelas aplicacións que están aloxadas na web e que nos permiten traballar sobre os nosos arquivos sen necesidade de depender dun determinado equipo, xa que dispoñendo dunha conexión a Internet, poderemos editalos e compartilos con outros usuarios accedendo a través do navegador sen necesidade de instalar as aplicacións tradicionais de escritorio, como _Microsoft Office, LibreOffice,_ _iWork_, etc…

As principais vantaxes das aplicacións ofimáticas na web son:

-   Soamente requiren unha conexión a Internet.
    
-   Accédese a través do navegador.
    
-   Facilidade de uso.
    
-   Permiten a creación de documentos de texto, follas de cálculo, presentacións, formularios, debuxos, etc…, soportando a maioría de formatos.
    
-   Os datos quedan almacenados nun servidor web ao que se pode acceder dende calquera dispositivo con acceso a Internet e por diferentes usuarios.
    
-   Permiten traballo colaborativo e edición simultánea de documentos.
    
-   Permiten integrar doadamente os arquivos, mediante enlazado ou código embebido, a calquera outro sitio online (web, blog,...).
    
-   O mantemento e as actualizacións son tarefas do provedor do servizo, non do usuario do mesmo.
    
-   Dispoñibles en varios idiomas.
    
-   A maioría son gratuítas.
    

E as principias desvantaxes son:

-   A velocidade de traballo depende da conexión a Internet dispoñible.
    
-   Problema de seguridade: Non se pode garantir a confiabilidade dos datos.
    

Na actualidade existen múltiples servizos gratuítos de aplicacións ofimáticas web, sendo os máis populares as seguintes:

-   **Google Drive** , de Google Inc. ([www.google.com/drive](http://www.google.com/drive))
    
-   **Zoho**, de Zoho Corporation Private Limited ([zoho.com](http://www.zoho.com/))
    
-   **OneDrive**, de Microsoft (www.onedrive.com)
    
## Google Drive
En [Google Workspace](https://workspace.google.com/intl/es-419_ar/) podemos usar un conxunto de aplicacións ofimáticas na nube (editor de texto, folla de cálculo, presentacións, formularios, debuxos…), que permite, ademais de crear novos arquivos, subir e compartir outros creados previamente con ferramentas de escritorio (offline), para editar de forma colaborativa.

As súas principais vantaxes son:

-   **Almacenamento online:** Permite acceder ao arquivos en calquera momento e en calquera lugar, o que che ofrece a posibilidade de traballar dende o teu posto de traballo, en casa, cando vas de un lugar a outro, a través do teléfono móbil e ata *sen conexión a Internet.*
    
-   **Compatible** **con todos** **os** **sistemas** **operativos:** Funciona no navegador de equipos PC, Mac e Linux, e é compatible con formatos comúns de Microsoft Office, como .docx, .xlsx, .pptx e .pdf, e con formatos de Openoffice/LibreOffice (.odt, .ods, .odp).
    
-   **Arquivos** **fáciles de crear, de subir** **e** **de compartir.**
    
-   **Aloxado de forma segura na Web**, xa que emprega cifrado SSL
    
-   **Realización dunha copia de seguridade online de todos os arquivos.**
    
-   **Control** **de acceso** **seguro:** Os administradores poden xestionar os permisos para o uso compartido de arquivos en todo o sistema, e os propietarios de documentos poden compartir e suspender en calquera momento o acceso aos arquivos.
    
-   **15** **GB** de almacenamento gratuíto por conta.


### Google Apps for Education 
É un programa de Google que permite ós centros educativos públicos dispoñer de unha serie de servizos de Google como:
-   Conta de correo: 30 GB de espazo de almacenamento.
-   Listas de correo de distribución por grupos.
-   Google Drive.
-   Google Docs.
-   Google Sites
-   Calendarios.
-   Ferramentas colaborativas
-   Acceso ós servizos mediante dispositivos móviles.
Máis info [aquí](https://support.google.com/a/users#topic=9393003)

**Acceso en modo off-line**

Google Drive dispón dunha aplicación para escritorio que instala un cartafol no teu equipo chamada _**Google Drive**_ e que se sincroniza automaticamente con Google Drive.

A vantaxe de instalar Google Drive é que podemos traballar con el en modo _offline_, de maneira que podemos crear novos documentos mediante as aplicacións ofimáticas que proporciona sen a necesidade de estar conectados a Internet.

Para poder traballar neste modo e que logo os arquivos se almacenen no Drive da nube, temos que gardar todos os arquivos que vaiamos creando, ou ben os que queiramos almacenar na nube, nos cartafoles de Google Drive creados na instalación. Deste xeito todos os arquivos que están en _Mi unidad_ de Google Drive sincronizaranse automaticamente e serán accesibles dende calquera dispositivo unha vez que dispoñamos de conexión a Internet.

Ademais, os cartafoles e arquivos estarán dispoñibles se nalgún momento non dispoñemos de conexión a Internet. A sincronización realizarase cada vez que se acceda a Google Drive a través da rede

## Zoho
Podemos rexistrarnos na web [www.zoho.com](http://www.zoho.com/) co usuario dunha conta de Google (p.ex. a de iessanclemente.net)
Ao acceder ao [Zoho workplace](https://www.zoho.com/es-xl/workplace/?src=zoho-homeireft=ohome) veremos a grande cantidade de aplicacións que ofrece, ademais da súa compatibilidade con distintos formatos ofimáticos.


