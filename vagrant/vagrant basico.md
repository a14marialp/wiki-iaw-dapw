﻿


> Written with [StackEdit](https://stackedit.io/).
> 
Comando para convertir el directorio actual en un entorno Vagrant -> crea el fichero vagrantfile si no existe

    vagrant init 
Comando para añadir boxes (MV)

    vagrant box add generic/centos7
    vagrant box add yko/ubuntu-xenial

los 2 comandos anteriores en 1 solo paso -> crear un vagrantfile con la box generic/centos7

    vagrant init generic/centos7
   
Y para levantar la box:

    vagrant up
Y para acceder a la box:

    vagrant ssh
Parar la box:

    vagrant halt
Eliminar la box:

    vagrant destroy

